const bg = document.querySelector('.bg-image');
const content = document.querySelector('.page-content');
const windowWidth = window.innerWidth / 5;
const windowHeight = window.innerHeight / 5 ;

content.addEventListener('mousemove', (e) => {
  const mouseX = e.clientX / windowWidth;
  const mouseY = e.clientY / windowHeight;
  bg.style.transform = `translate3d(-${mouseX*2}%, -${mouseY*2}%, 0)`;
});

document.getElementById('copyyear').innerHTML = new Date().getFullYear();
